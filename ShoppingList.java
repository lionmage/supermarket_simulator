import java.lang.Math;
import java.util.*;

class ShoppingList {

	public ArrayList<String> productsToVisit = new ArrayList<String>();
	public int custAge;
	public int custWealth;
	public int timeOfDay;

	public ShoppingList(int custAge, int custWealth, int timeOfDay) {
		this.custAge 		 = custAge;
		this.custWealth 	 = custWealth;
		this.timeOfDay  	 = timeOfDay;
		//this.productsToVisit = populateShoppingList(custAge, custWealth, timeOfDay);
	}

	public ArrayList<String> populateShoppingList(int custAge, int custWealth, int timeOfDay) {
		ArrayList<String> output = new ArrayList<String>();
		// Meal Deals
		if (isSuccess("MLD0000", custAge, custWealth, timeOfDay)) {
			// Sandwiches
			if (isSuccess("MLD0100", custAge, custWealth, timeOfDay)) {
				// Prawn
				if (isSuccess("MLD0101", custAge, custWealth, timeOfDay)) {
					// Add prawn sandwich to list.
				}
				// Chicken
				if (isSuccess("MLD0102", custAge, custWealth, timeOfDay)) {
					// Add chicken sandwich to list.
				}
				// BLT
				if (isSuccess("MLD0103", custAge, custWealth, timeOfDay)) {
					// Add BLT sandwich to list.
				}
			}
			// Crisps
			if (isSuccess("MLD0200", custAge, custWealth, timeOfDay)) {
				// Salt
				if (isSuccess("MLD0201", custAge, custWealth, timeOfDay)) {
					// Add salt crisps to list.
				}
				// Vinegar
				if (isSuccess("MLD0202", custAge, custWealth, timeOfDay)) {
					// Add vinegar crisps to list.
				}
				// Cheese
				if (isSuccess("MLD0203", custAge, custWealth, timeOfDay)) {
					// Add cheese crisps to list.
				}
				
			}
			// Drinks
			if (isSuccess("MLD0300", custAge, custWealth, timeOfDay)) {
				// Water
				if (isSuccess("MLD0301", custAge, custWealth, timeOfDay)) {
					// Add water list.
				}
				// Coke
				if (isSuccess("MLD0302", custAge, custWealth, timeOfDay)) {
					// Add Coke to list.
				}
				// Orange
				if (isSuccess("MLD0303", custAge, custWealth, timeOfDay)) {
					// Add orange to list.
				}
			}
		}
		// Dairy
		if (isSuccess("DAR0000", custAge, custWealth, timeOfDay)) {
			//Milk
			if (isSuccess("DAR0100", custAge, custWealth, timeOfDay)) {
				// Add milk to list
			}
			//Cheese
			if (isSuccess("DAR0200", custAge, custWealth, timeOfDay)) {
				if (isSuccess("DAR0201", custAge, custWealth, timeOfDay)) {
					// Add cheddar to list
				}
				if (isSuccess("DAR0202", custAge, custWealth, timeOfDay)) {
					//Add mozzarella to list
				}
			}
			//Eggs
			if (isSuccess("DAR0300", custAge, custWealth, timeOfDay)) {
					// Add eggs to list
			}
		}
		// Meat
		if (isSuccess("MET0000", custAge, custWealth, timeOfDay)) {
			// Steak
			if (isSuccess("MET0100", custAge, custWealth, timeOfDay)) {
				// Add steak to list
			}
			// Mince
			if (isSuccess("MET0200", custAge, custWealth, timeOfDay)) {
				// Add mince to list
			}
			// Pork
			if (isSuccess("MET0300", custAge, custWealth, timeOfDay)) {
				// Add pork to list
			}
			// Chicken 
			if (isSuccess("MET0400", custAge, custWealth, timeOfDay)) {
				// Add chicken to list
			}
			// Salmon
			if (isSuccess("MET0500", custAge, custWealth, timeOfDay)) {
				// Add salmon to list
			}
			// Tuna 
			if (isSuccess("MET0600", custAge, custWealth, timeOfDay)) {
				// Add Tuna to list
			}
		}
		// Fruit and vegetables 
		if (isSuccess("VEG0000", custAge, custWealth, timeOfDay)) {
			// Onions
			if (isSuccess("VEG0100", custAge, custWealth, timeOfDay)) {
				// Add onions to list
			}
			// Garlic
			if (isSuccess("VEG0200", custAge, custWealth, timeOfDay)) {
				// Add garlic to list
			}
			// Carrots
			if (isSuccess("VEG0300", custAge, custWealth, timeOfDay)) {
				// Add carrots to list
			}
			// Bananas
			if (isSuccess("VEG0400", custAge, custWealth, timeOfDay)) {
				// Add bananas to list
			}
			// Apples
			if (isSuccess("VEG0500", custAge, custWealth, timeOfDay)) {
				// Add apples to list
			}
			// Potatoes
			if (isSuccess("VEG0600", custAge, custWealth, timeOfDay)) {
				// Add potatoes to list
			}
		}
		// Household goods
		if (isSuccess("HSG0000", custAge, custWealth, timeOfDay)) {
			// Toilet roll
			if (isSuccess("HSG0100", custAge, custWealth, timeOfDay)) {
				// Add toliet roll to list
			}
			// Kitchen roll
			if (isSuccess("HSG0200", custAge, custWealth, timeOfDay)) {
				// Add kitchen roll to list
			}
			// Tin foil
			if (isSuccess("HSG0300", custAge, custWealth, timeOfDay)) {
				// Add tin foil to list
			}
			// Soap
			if (isSuccess("HSG0300", custAge, custWealth, timeOfDay)) {
				// Add soap to list
			}
			// Dish Soap
			if (isSuccess("HSG0300", custAge, custWealth, timeOfDay)) {
				// Add dish soap to list
			}
			// Bleach
			if (isSuccess("HSG0300", custAge, custWealth, timeOfDay)) {
				// Add bleach to list
			}
		}
		// Bakery
		if (isSuccess("BAK0000", custAge, custWealth, timeOfDay)) {
			// Donuts
			if (isSuccess("BAK0100", custAge, custWealth, timeOfDay)) {
				// Add toilet roll to list
			}
			// Bread loaves
			if (isSuccess("HSG0200", custAge, custWealth, timeOfDay)) {
				// White
				if (isSuccess("BAK0201", custAge, custWealth, timeOfDay)) {
				// Add white bread to list
				}
				// Brown
				if (isSuccess("BAK0202", custAge, custWealth, timeOfDay)) {
				// Add brown to list
				
				}
				// 50-50
				if (isSuccess("BAK0203", custAge, custWealth, timeOfDay)) {
				// Add 50-50 to list
				}
			}
			// Cookies
			if (isSuccess("BAK0300", custAge, custWealth, timeOfDay)) {
				// Add cookies to list
			}
		}
		// Cereals 
		if (isSuccess("CER0000", custAge, custWealth, timeOfDay)) {
			// Rice
			if (isSuccess("CER0100", custAge, custWealth, timeOfDay)) {
				// Add rice to list
			}
			// Breakfast cereals
			if (isSuccess("CER0200", custAge, custWealth, timeOfDay)) {
				// ChocPops
				if (isSuccess("CER0201", custAge, custWealth, timeOfDay)) {
					// Add ChocPops to list
				}
				// SugarPuffs
				if (isSuccess("CER0202", custAge, custWealth, timeOfDay)) {
					// Add SUgarPuffs to list
				}
				// Shreddies
				if (isSuccess("CER0203", custAge, custWealth, timeOfDay)) {
					// Add Shreddies to list
				}
			}
			// Pasta
			if (isSuccess("CER0300", custAge, custWealth, timeOfDay)) {
				// Brown
				if (isSuccess("CER0301", custAge, custWealth, timeOfDay)) {
					// Add brown pasta to list
				}
				if (isSuccess("CER0302", custAge, custWealth, timeOfDay)) {
					// Add white pasta to list
				}
			}
		}
		// Confectionary
		if (isSuccess("CFY0000", custAge, custWealth, timeOfDay)) {
			// Digestive
			if (isSuccess("CFY0100", custAge, custWealth, timeOfDay)) {
				// Add digestive to list.
			}
			// Choc Digestive
			if (isSuccess("CFY0200", custAge, custWealth, timeOfDay)) {
				// Add choc digestive to list.
			}
			// Choc Bar
			if (isSuccess("CFY0300", custAge, custWealth, timeOfDay)) {
				// Twix
				if (isSuccess("CFY0301", custAge, custWealth, timeOfDay)) {
					// Add twix to list.
				}
				// KitKat
				if (isSuccess("CFY0302", custAge, custWealth, timeOfDay)) {
					// Add kitkat to list.
				}
			}
			// Gummy Sweets
			if (isSuccess("CFY0400", custAge, custWealth, timeOfDay)) {
				// Add gummy sweets to list.
			}
			// Selection Box
			if (isSuccess("CFY0500", custAge, custWealth, timeOfDay)) {
				// Celebrations
				if (isSuccess("CFY0501", custAge, custWealth, timeOfDay)) {
					// Add celebrations to list.
				}
				// Heros
				if (isSuccess("CFY0502", custAge, custWealth, timeOfDay)) {
					// Add heros to list.
				}
			}
		}
		// Alcohol
		if (isSuccess("ALC0000", custAge, custWealth, timeOfDay)) {
			// Wine
			if (isSuccess("ALC0100", custAge, custWealth, timeOfDay)) {
				// Red
				if (isSuccess("ALC0101", custAge, custWealth, timeOfDay)) {
					// Add red wine to list.
				}
				// White
				if (isSuccess("ALC0102", custAge, custWealth, timeOfDay)) {
					// Add white wine to list.
				}
			}
			// Spirits
			if (isSuccess("ALC0200", custAge, custWealth, timeOfDay)) {
				// Vodka
				if (isSuccess("ALC0201", custAge, custWealth, timeOfDay)) {
					// Add vodka to list.
				}
				// Whiskey
				if (isSuccess("ALC0202", custAge, custWealth, timeOfDay)) {
					// Add whiskey to list.
				}
			}
			// Beer
			if (isSuccess("ALC0300", custAge, custWealth, timeOfDay)) {
				// Add beer to list.
			}
			// Cider
			if (isSuccess("ALC0400", custAge, custWealth, timeOfDay)) {
				// Add cider to list.
			}
		}
		
		return output;
	}

	public boolean isSuccess(String productID, int custAge, int custWealth, int timeOfDay) {
		int productNumber = Integer.parseInt(productID.substring(3));
		int probability;
		// Meal Deal - TOD
		if (productID.substring(0,2).equals("MLD")) {
			
		}
		// Dairy - AGE
		if (productID.substring(0,2).equals("DAR")) {

		}
		// Meat - TOD; WEALTH; AGE
		if (productID.substring(0,2).equals("MET")) {

		}
		// Fruit & Veg - AGE
		if (productID.substring(0,2).equals("VEG")) {

		}
		// Household Goods - AGE
		if (productID.substring(0,2).equals("HSG")) {

		}
		// Bakery - TOD
		if (productID.substring(0,2).equals("BAK")) {

		}
		// Cereals
		if (productID.substring(0,2).equals("CRL")) {

		}
		// Confectionary - WEALTH
		if (productID.substring(0,2).equals("CFY")) {

		}
		// Alcohol - TOD; AGE; WEALTH
		if (productID.substring(0,2).equals("ALC")) {

		}
		return false; // TEMP
	}

	public double calculatePolynomial(double x, double x6, double x5, double x4, double x3, double x2, double x1, double x0) {
		double y;
		y = x6*(Math.pow(x,6)) + x5*(Math.pow(x,5)) + x4*(Math.pow(x,4)) + x3*(Math.pow(x,3)) + x2*(Math.pow(x,2)) + x1*(Math.pow(x,1)) + x0;
		return y;
	}

}