import java.util.*;

class Customer {

	public ShoppingList myShoppingList;
	public int custAge;
	public int custClass;
	public int currentTime;

	public Customer(int custAge, int custClass, int time) {
		this.myShoppingList = new ShoppingList(custAge, custClass, time);
		this.custAge 		= custAge;
		this.custClass 		= custClass;
		this.currentTime 	= time;
	}

	public void arriveAtProduct(String productID, int time) {
		this.currentTime 	= time;
		//this.myShoppingList.productAdded(productID);
	}

}